メリュ
=======

メリュ (Meyou) est un simulateur d'évolution, réalisé dans le cadre de l'UE LIFPROJET en 3e année de license d'informatique.
Le projet est construit autour de deux fonctionnalités principales:
- Simuler l'évolution des créatures du nom de "Meyoux" en les sélectionnant via un critère arbitraire
- Créer des meyoux à la main, dans un éditeur, afin de les tester ensuite lors d'une évolution

Installation
--------

Vous pouvez télécharger le code source avec la commande suivante:

    git clone https://forge.univ-lyon1.fr/p2100741/amfc-lifprojet.git

Compilation
-------

Utilisez Unity 2022.3.9f1 pour compiler le projet. en utilisant l'onglet `File` puis `Build Settings` et enfin `Build`.

Présentation
-------
### Menu
Quand vous lancez le jeu, vous arrivez sur un menu avec plusieurs boutons:
voici une capture d'écran du menu:

![Menu](./screenshot/menu.png)

- `évoluer` renvoie vers une page qui permet de choisir les paramètres de l'évolution
- `créer meyou` permet de créer un meyou à la main et le sauvegarder

### Paramètre d'évolution

Il s'agit d'une page qui permet de choisir les paramètres de l'évolution:

![Paramètre d'évolution](./screenshot/evolution.png)

- `Type d'évolution` permet de choisir entre la course et le saut
- `Fichier de statistiques` permet de choisir le fichier de statistiques à utiliser si il est vide, rien ne se passe
- `Durée de génération` permet de choisir la durée de chaque génération
- `Meyou` permet de choisir le meyou à faire évoluer parmis les sauvegardés
- `Dossier snapshots` si un chemin est spécifié alors le logiciel va sauvegarder le meilleur meyous de la génération
- `Taille de la génération` permet de choisir la taille de la génération
- `Méthode d'évolution` permet de choisir la méthode qui va être choisi pour l'évolution entre juste mouvement ou modification du corps et mouvement
- `Fréquence des snapshots` permet de choisir le nombre de génération entre chaque snapshot
- `Taille du groupe` permet si elle est inferieur à la taille de la génération de faire évoluer un certain nombre de meyous en même temps et faire chaque génération en plusieur fois

### Évolution

Il s'agit de la page principale du logiciel, elle permet de voir les meyous évoluer:

![Évolution](./screenshot/evolution2.png)

### Création de meyou

Il s'agit d'une page qui permet de créer un meyou à la main:

![Création de meyou](./screenshot/creation.png)
![Création de meyou 2](./screenshot/creation2.png)

Ressources externes utilisées
-------

- Unity 2022.3.9f1
    <https://unity.com/fr>
- Unity-chan! Model
    <https://unity-chan.com/>
- Json.NET
    <https://www.newtonsoft.com/json>