using System;
using System.Collections.Generic;
using System.Linq;
using Meyou;
using Mutation;
using TMPro;
using Trials;
using Trials.Generators;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Menus
{
    public class EvolutionConfigMenu : MonoBehaviour
    {
        public TMP_Dropdown trialInput;
        public TMP_Dropdown meyouInput;
        public TMP_Dropdown evolutionInput;
        public TMP_InputField statsOutputInput;
        public TMP_InputField snapshotOutputInput;
        public TMP_InputField snapshotFreqInput;
        public TMP_InputField genDurationInput;
        public TMP_InputField genSizeInput;
        public TMP_InputField bulkSizeInput;

        private Gene[] availableGenes;

        // Start is called before the first frame update
        void Start()
        {
            // Récupère tous les Meyoux sauvegardés et créé les options nécessaires
            var (genes, geneDates) = SaveFile.LoadGene();
            availableGenes = genes;

            meyouInput.ClearOptions();

            var options = genes
                .Select((t, i) => $"{t.Name} {geneDates[i]}")
                .Select(txt => new TMP_Dropdown.OptionData(txt))
                .ToList();

            meyouInput.AddOptions(options);
        }


        // Update is called once per frame
        void Update()
        {
        
        }



        public void StartEvolution()
        {
            SetLoadInfo();

            var scene = trialInput.value switch
            {
                0 => "RangeTrial",
                1 => "JumpTrial",
                _ => throw new ArgumentException("Paramètre Trial invalide")
            };

            SceneManager.LoadScene(scene);
        }



        private void SetLoadInfo()
        {
            EvolutionLoadInfo.InitialGene = availableGenes[meyouInput.value];

            EvolutionLoadInfo.EvolverType = evolutionInput.value switch
            {
                0 => EvolverType.Movement,
                1 => EvolverType.BodyAndMovement,
                _ => throw new ArgumentException("Paramère Evolver invalide")
            };

            EvolutionLoadInfo.SnapshotPath = snapshotOutputInput.text;
            EvolutionLoadInfo.StatsOutputFile = statsOutputInput.text;

            EvolutionLoadInfo.SnapshotRate = int.Parse(snapshotFreqInput.text);
            EvolutionLoadInfo.GenerationDuration = uint.Parse(genDurationInput.text);
            EvolutionLoadInfo.GenerationSize = uint.Parse(genSizeInput.text);
            EvolutionLoadInfo.BulkSize = uint.Parse(bulkSizeInput.text);

        }
    }
}
