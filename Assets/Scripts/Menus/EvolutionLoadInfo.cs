﻿using System;
using System.IO;
using Meyou;
using Trials;
using Trials.Generators;
using UnityEngine;

namespace Menus
{
    /// <summary>
    /// Classe statique permettant de transmettre les paramètres
    /// aux scènes Trials.
    /// </summary>
    public static class EvolutionLoadInfo
    {
        private static string _snapshotPath = "";
        private static int _snapshotRate = 1;
        private static uint _generationDuration = 2;
        private static uint _generationSize = 500;
        private static uint _bulkSize = 100;

        private static Gene _initialGene;


        public static string StatsOutputFile { get; set; } = "";

        public static EvolverType EvolverType { get; set; } = EvolverType.BodyAndMovement;

        public static string SnapshotPath
        {
            get => _snapshotPath;
            set
            {
                if (!Directory.Exists(value) && value != "") throw new ArgumentException("Le dossier de snapshot n'existe pas");
                _snapshotPath = value;
            }
        }

        public static int SnapshotRate
        {
            get => _snapshotRate;
            set
            {
                if (value <= 0) throw new ArgumentException("La fréquence de snapshot doit être > 0");
                _snapshotRate = value;
            }
        }

        public static uint GenerationDuration
        {
            get => _generationDuration;
            set
            {
                if (value <= 0) throw new ArgumentException("La durée de la génération doit être > 0");
                _generationDuration = value;
            }
        }

        public static uint GenerationSize
        {
            get => _generationSize;
            set
            {
                if (value <= 1) throw new ArgumentException("La taille de la génération doit être > 1");
                _generationSize = value;
            }
        }

        public static uint BulkSize
        {
            get => _bulkSize;
            set
            {
                if (value <= 0) throw new ArgumentException("La taille des groupes doit être > 0");
                _bulkSize = value;
            }
        }

        public static Gene InitialGene
        {
            get
            {
                _initialGene ??= Gene.DebugGetSample();
                return _initialGene;
            }
            set => _initialGene = value;
        }
    }
}