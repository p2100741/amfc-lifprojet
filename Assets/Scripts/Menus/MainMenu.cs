using System.Collections;
using Meyou;
using Trials;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Menus
{
    public class MainMenu : MonoBehaviour
    {
        public MeyouSpawner spawner;

        // Start is called before the first frame update
        void Start()
        {
            SaveFile.SaveBuiltinGenes();
            StartCoroutine(SpawnNextFrame());
        }



        public void LoadEvolutionMenuScene()
        {
            SceneManager.LoadScene("EvolutionConfigMenu");
        }

        public void LoadEditorScene()
        {
            SceneManager.LoadScene("Editor");
        }



        private IEnumerator SpawnNextFrame()
        {
            yield return null;

            // Récupère un gène sauvegardé aléatoire pour l'afficher
            var (genes, _) = SaveFile.LoadGene();

            var rdm = new System.Random();
            var gene = genes[rdm.Next(0, genes.Length)];

            spawner.SpawnOneRawMeyou(gene);
        }
    }
}
