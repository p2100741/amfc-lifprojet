using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Meyou;
using UnityEngine;

public static class SaveFile
{
    private const string GeneFolderName = "Meyou";
    private static string _geneFolderPath = "";



    public static void SaveGene(Gene gene)
    {
        // check if directory exists
        if (!Directory.Exists(GetSaveFolder())) Directory.CreateDirectory(GetSaveFolder());

        // serialize the gene
        string json = Newtonsoft.Json.JsonConvert.SerializeObject(gene);

        // get the current date and time
        DateTime now = DateTime.Now;

        // create the file name with the date and time
        string fileName = gene.Name + "_" + now.ToString("yyyy-MM-dd_HH-mm-ss") + "_Meyou.json";
        string filePath = Path.Combine(GetSaveFolder(), fileName);


        File.WriteAllText(filePath, json);
    }



    /**
     * Copie les gènes présents dans le dossier /Meyoux vers le dossier de sauvegarde du jeu.
     */
    public static void SaveBuiltinGenes()
    {
        var savePath = GetSaveFolder();
        // check if directory exists
        if (!Directory.Exists(savePath))
        {
            Directory.CreateDirectory(savePath);
        }

        // Get each file in the /Meyoux directory
        var meyoux = Resources
            .LoadAll("Meyoux", typeof(TextAsset))
            .Cast<TextAsset>()
            .ToArray();

        foreach (var meyou in meyoux)
        {
            var fileName = Path.Combine(savePath, meyou.name);
            File.WriteAllText(fileName, meyou.text);
            //File.Copy(, Path.Combine(savePath, file.Name), true);
        }
    }


    public static (Gene[], string[]) LoadGene()
    {
        // load all the files and order them by date
        var fileContents = LoadFilesByTime(GetSaveFolder());

        // make them into genes
        Gene[] genes = new Gene[fileContents.Count];
        string[] dates = new string[fileContents.Count];
        int loopCount = 0;

        foreach (var content in fileContents)
        {
            genes[loopCount] = Newtonsoft.Json.JsonConvert.DeserializeObject<Gene>(content.Item1);
            dates[loopCount] = content.Item2.ToString("dd/MM/yy H:mm");
            loopCount++;
        }

        return (genes, dates);
    }

    private static List<(string, DateTime)> LoadFilesByTime(string directoryPath)
    {
        List<(string, DateTime)> fileContents = new List<(string, DateTime)>();

        try
        {
            // Get all files in the directory
            DirectoryInfo directory = new DirectoryInfo(directoryPath);
            FileInfo[] files = directory.GetFiles();

            // Order files by last modified time
            files = files.OrderByDescending(f => f.LastWriteTime).ToArray();

            // Read content of each file
            foreach (var file in files)
            {
                fileContents.Add((File.ReadAllText(file.FullName), File.GetLastWriteTime(file.FullName)));
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occurred: {ex.Message}");
        }

        return fileContents;
    }




    private static string GetSaveFolder()
    {
        if (_geneFolderPath == "")
        {
            _geneFolderPath = Path.Combine(Application.persistentDataPath, GeneFolderName);
        }

        return _geneFolderPath;
    }
}