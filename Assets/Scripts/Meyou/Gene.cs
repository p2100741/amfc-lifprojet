﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using UnityEngine;

namespace Meyou
{
    public enum PartPos
    {
        PosX = 0,
        NegX = 1,
        PosY = 2,
        NegY = 3, // seulement la tête
        PosZ = 4,
        NegZ = 5
    }


    /// Représente les données génétiques d'un Meyou
    /// sous forme d'arbre.
    public class Gene : ICloneable
    {
        /// Génération de laquelle est issu ce MeyouGene
        public int Generation { get; set; }

        /// Nom du Meyou
        public String Name { get; set; }

        /// Nombres de positions différentes qui composent un mouvement
        /// c-à-d l'animation qui sera répétée par l'individu en boucle.
        /// Plus le nombre est grand, plus le mouvement pourra être précis
        /// MAIS sera + long à évoluer et représentera + de données.
        ///
        /// Ce nombre comprend la position initiale, ainsi _nbPosition >= 1.
        private int _nbPositions = 1;

        public int NbPositions
        {
            get => _nbPositions;
            set { if (value >= 1) _nbPositions = value; }
        }

        /// Durée du mouvement, en secondes. Ainsi, si _movementSpeed = 1
        /// et _nbPositions = 4, le Meyou passera d'une position à une autre
        /// en 0.25s.
        private float _movementSpeed = 1;

        public float MovementSpeed
        {
            get => _movementSpeed;
            set { if (value > 0) _movementSpeed = value; }
        }

        /// Tête du Meyou, c-à-d la partie de son corps qui est racine de l'arbre
        /// le représentant.
        public PartGene Head { get; set; }


        public Gene(int nbPositions, float movementSpeed, string name)
        {
            NbPositions = nbPositions;
            MovementSpeed = movementSpeed;
            Name = name;
            Generation = 0;
        }


        [JsonConstructor]
        public Gene(string name, int generation, int nbPositions, float movementSpeed, PartGene head)
        {
            Name = name;
            Generation = generation;
            NbPositions = nbPositions;
            MovementSpeed = movementSpeed;
            Head = head;
        }




        /// <summary>
        /// Renvoie un Gene de Meyou à des fins de debug.
        /// </summary>
        public static Gene DebugGetSample()
        {
            var positions = new List<SerializableVector2>
            {	
                new Vector2(0, 0),
                new Vector2(0, 0),
                new Vector2(0, 0),
                new Vector2(0, 0)
            };

            var gene = new Gene(positions.Count, 0.5f, "DebugMeyou");
            var geneHead = new PartGene(1, 1, 1);
            var geneTorso = new PartGene(2, 2, 2);
            var geneAbdomen = new PartGene(2, 2, 2);
            var geneArmLeft = new PartGene(0.2f, 3f, 0.2f);
            var geneArmRight = new PartGene(0.2f, 3f, 0.2f);

            geneTorso.AddConnection(PartPos.PosX, new PartConnection(positions, geneArmLeft, 1.0f));
            geneTorso.AddConnection(PartPos.NegX, new PartConnection(positions, geneArmRight, 1.0f));

            geneAbdomen.AddConnection(PartPos.PosX, new PartConnection(positions, geneArmLeft, 1.0f));
            geneAbdomen.AddConnection(PartPos.NegX, new PartConnection(positions, geneArmRight, 1.0f));

            geneTorso.AddConnection(PartPos.PosY, new PartConnection(positions, geneAbdomen, 1.0f));


            geneHead.AddConnection(
                PartPos.NegZ,
                new PartConnection(positions, geneTorso, 1.0f)
            );

            gene.Head = geneHead;

            return gene;
        }



        /// <summary>
        /// Ecrit dans le fichier Assets/Meyoux/test.json le Json
        /// du Gene de test issu de DebugGetSample()
        /// </summary>
        public static void DebugWriteJson()
        {
            var settings = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                NullValueHandling = NullValueHandling.Include
            };

            var json = JsonConvert.SerializeObject(DebugGetSample(), settings);

            StreamWriter writer = new StreamWriter("Assets/Meyoux/test.json");
            writer.Write(json);
            writer.Close();
        }


        public object Clone()
        {
            return new Gene(_nbPositions, _movementSpeed, Name)
            {
                Generation = Generation,
                Head = (PartGene)Head.Clone(),
            };
        }



        /// <summary>
        /// Renvoie l'énergie totale consommée par ce Meyou
        /// </summary>
        /// <returns></returns>
        public float GetTotalEnergy()
        {
            Stack<PartGene> bodyPartStack = new Stack<PartGene>();
            bodyPartStack.Push(Head);

            var res = 0.0f;

            while (bodyPartStack.Count > 0)
            {
                var current = bodyPartStack.Pop();
                res += current.GetEnergy();
                foreach (var conn in current.connections)
                {
                    bodyPartStack.Push(conn.Value.Part);
                }
            }

            return res;
        }
    }





    /// Représente de manière récursive les parties du corps d'un Meyou.
    public class PartGene : ICloneable
    {
        public float ScaleX { get; set; }
        public float ScaleY { get; set; }
        public float ScaleZ { get; set; }

        // Doit être public pour être sérialisé par Json.NET
        public Dictionary<PartPos, PartConnection> connections;


        public Vector3 GetScale()
        {
            return new Vector3(ScaleX, ScaleY, ScaleZ);
        }


        public PartGene(float scaleX, float scaleY, float scaleZ)
        {
            ScaleX = scaleX;
            ScaleY = scaleY;
            ScaleZ = scaleZ;

            connections = new Dictionary<PartPos, PartConnection>();
        }


        [JsonConstructor]
        public PartGene(float scaleX, float scaleY, float scaleZ, Dictionary<PartPos, PartConnection> connections)
        {
            ScaleX = scaleX;
            ScaleY = scaleY;
            ScaleZ = scaleZ;

            this.connections = connections;
        }


        public void AddConnection(PartPos connector, PartConnection connection)
        {
            connections.Add(connector, connection);
        }

        public object Clone()
        {
            var partGene = new PartGene(ScaleX, ScaleY, ScaleZ);
            foreach (var couple in connections)
            {
                partGene.AddConnection(couple.Key, (PartConnection)couple.Value.Clone());
            }

            return partGene;
        }

        /// <summary>
        /// Renvoie true lorsque ce PartGene n'a aucune connexion enfant.
        /// </summary>
        public bool IsLeaf()
        {
            return connections.Count == 0;
        }


        /// <summary>
        /// Renvoie la mesure de l'énergie consommée par ce BodyPart.
        /// </summary>
        public float GetEnergy()
        {
            var res = 0.0f;
            foreach (var connection in connections)
            {
                res += connection.Value.forceFactor * connection.Value.GetDistance();
            }

            return res;
        }
    }




    public class PartConnection : ICloneable
    {
        public List<SerializableVector2> Positions;
        public float forceFactor = 1.0f;
        public PartGene Part;

        [JsonConstructor]
        public PartConnection(List<SerializableVector2> positions, PartGene part, float force)
        {
            Positions = positions;
            Part = part;
            forceFactor = force;
        }

        /// effectue une copie profonde de PartConnection
        /// Renvoie un PartConnection
        public object Clone()
        {
            var newList = Positions.Select(e => (SerializableVector2)e.Clone()).ToList();
            return new PartConnection(newList, (PartGene)Part.Clone(), forceFactor);
        }


        public float GetDistance()
        {
            var res = 0.0f;
            for (int i = 0; i < Positions.Count; i++)
            {
                var v1 = (Vector2) Positions[i];
                var v2 = (Vector2)Positions[(i + 1) % Positions.Count];
                res += (v2 - v1).magnitude;
            }

            return res;
        }
    }
}