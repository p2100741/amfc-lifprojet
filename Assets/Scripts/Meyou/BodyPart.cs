using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Meyou
{
    public class BodyPart : MonoBehaviour
    {
        protected Dictionary<PartPos, BodyPart> connectedBodies;

        protected Dictionary<PartPos, ConfigurableJoint> configJoints;

        protected Dictionary<PartPos, float> forces;

        protected bool isConfigured;

        // >=1, c.f Meyou.Gene
        private int _nbPositions;

        public int NbPositions
        {
            get => _nbPositions;
            set { if (value >= 1) _nbPositions = value; }
        }


        // Stocke les positions que doivent adopter les articulations
        // de ce BodyPart au fur et à mesure de l'animation.
        protected Dictionary<PartPos, List<Vector2>> targets;

        protected void OnEnable()
        {
            configJoints = new Dictionary<PartPos, ConfigurableJoint>();
            targets = new Dictionary<PartPos, List<Vector2>>();
            connectedBodies = new Dictionary<PartPos, BodyPart>();
            forces = new Dictionary<PartPos, float>();
        }






        /// Défini les positions que doit prendre l'articulation à la position PartPos
        /// au cours de l'animation.
        public virtual void AddAnimationStep(PartPos connector, List<Vector2> positions)
        {
            if (connector == PartPos.NegY)
                throw new ApplicationException("BodyPart ne peut pas avoir de connexion en NegY");

            if (positions.Count != _nbPositions)
                throw new ArgumentException("Le nombre de positions données doit être égal à NbPositions");

            if (!connectedBodies.ContainsKey(connector))
                throw new ArgumentException("NO");

            targets.Add(connector, positions);
        }


        /// <summary>
        /// Renvoie la position moyenne de tous les fils de ce BodyPart,
        /// y compris lui-même, dans l'espace global. Cette fonction ne
        /// doit pas être appelée avant que le Meyou n'ait été construit.
        /// </summary>
        /// <returns></returns>
        public Vector3 GetAveragePosition()
        {
            var avg = transform.position;

            foreach (var part in connectedBodies)
                avg += part.Value.GetAveragePosition();

            avg /= (1 + connectedBodies.Count);

            return avg;
        }


        /// Configure la BodyPart.
        /// Doit être appelée après toutes les connexions effectuées
        /// et SURTOUT après avoir appelé PopulateConfigJoints
        public void Configure()
        {
            if (isConfigured) throw new ApplicationException("Configuration d'une BodyPart déjà configurée");

            // Deep-copy de configJoints pour ne pas avoir à modifier le dictionnaire sur
            // lequel on itère
            var jointsCopy = configJoints.ToDictionary(
                entry => entry.Key,
                entry => entry.Value);

            foreach (var joint in jointsCopy)
            {
                if (connectedBodies.TryGetValue(joint.Key, out var body))
                {
                    var force = forces[joint.Key];
                    _ConfigConfigurableJoint(joint.Value, force);

                    joint.Value.connectedBody = body.GetComponent<Rigidbody>();
                }
                else
                {
                    configJoints.Remove(joint.Key);
                    Destroy(joint.Value);
                }
            }

            isConfigured = true;
        }



        public virtual void PopulateConfigJoints()
        {
            // Configure la Bodypart, supprime les connexions inutiles
            // (ordre garanti par la structure du Prefab BodyPart)
            if (isConfigured) return;

            var joints = transform.GetComponents<ConfigurableJoint>();
            configJoints.Add(PartPos.PosX, joints[0]);
            configJoints.Add(PartPos.NegX, joints[1]);
            configJoints.Add(PartPos.PosY, joints[2]);
            configJoints.Add(PartPos.PosZ, joints[3]);
            configJoints.Add(PartPos.NegZ, joints[4]);
        }




        /// Ajoute une connexion à ce BodyPart
        public virtual void AddConnectedBody(PartPos connector, BodyPart part, float force)
        {
            if (connectedBodies.ContainsKey(connector))
                throw new ArgumentException("Une connexion existe déjà sur ce connecteur");

            if (connector == PartPos.NegY)
                throw new ArgumentException("Les BodyPart (excepté Head) ne peuvent pas être connectés via NegY");

            connectedBodies.Add(connector, part);
            forces.Add(connector, force);
        }



        /*
        /// <summary>
        /// Renvoie un dictionnaire associant à chaque "connecteur" utilisé
        /// sa rotation cible.
        /// </summary>
        protected virtual Dictionary<PartPos, Vector2> GetTargets()
        {
            var dict = new Dictionary<PartPos, Vector2>();

            if (connectedBodyPosX != null) dict.Add(PartPos.PosX, targetPosX);
            if (connectedBodyNegX != null) dict.Add(PartPos.NegX, targetNegX);
            if (connectedBodyPosY != null) dict.Add(PartPos.PosY, targetPosY);
            if (connectedBodyPosZ != null) dict.Add(PartPos.PosZ, targetPosZ);
            if (connectedBodyNegZ != null) dict.Add(PartPos.NegZ, targetNegZ);

            return dict;
        }*/


        /// <summary>
        /// Met à jour les positions cibles des articulations
        /// de ce BodyPart et de ses "enfants" pour correspondre
        /// à la position à l'index donné
        /// </summary>
        public void SetPosition(int p)
        {
            foreach (var pos in targets)
            {
                // Convertit le Vector2 en Quaternion
                var clampedVector = Vector2To3OnZ(ClampVector(pos.Value[p], 90));
                var quat = Quaternion.Euler(clampedVector);

                configJoints[pos.Key].targetRotation = quat;
            }

            foreach (var child in connectedBodies)
                child.Value.SetPosition(p);
        }




        /// <summary>
        /// Défini la visibilité de ce Bodypart et des bodyparts enfants
        /// true = visible
        /// false = caché
        /// </summary>
        public void SetVisibility(bool visible)
        {
            var renderer = GetComponent<MeshRenderer>();
            renderer.enabled = visible;

            foreach (var child in connectedBodies)
            {
                child.Value.SetVisibility(visible);
            }
        }





        private static void _ConfigConfigurableJoint(ConfigurableJoint joint, float force)
        {
            joint.rotationDriveMode = RotationDriveMode.Slerp;

            var slerp = joint.slerpDrive;
            slerp.positionSpring = 10000 * force;
            slerp.positionDamper = 500 * force;
            slerp.maximumForce = 100 * force;
            joint.slerpDrive = slerp;
        }




        /// <summary>
        /// Renvoie un Vector2 dont les composant ne dépassent pas [-limit; limit]
        /// </summary>
        /// <param name="v">Le Vector2 à "clamper"</param>
        /// <param name="limit">La limite souhaitée, >=0</param>
        /// <returns></returns>
        private static Vector2 ClampVector(Vector2 v, float limit)
        {
            if (limit < 0) throw new ArgumentException("limit doit être nulle ou positive");
            return new Vector2(
                Mathf.Clamp(v.x, -limit, limit),
                Mathf.Clamp(v.y, -limit, limit)
            );
        }



        /// <summary>
        /// Renvoie un Vector3 à partir des componantes x & y d'un Vector2 selon Vector3(x, 0, y).
        /// </summary>
        /// <param name="vec">Vector2</param>
        /// <returns>Vector3</returns>
        private static Vector3 Vector2To3OnZ(Vector2 vec)
        {
            return new Vector3(vec.x, 0, vec.y);
        }
    }
}
