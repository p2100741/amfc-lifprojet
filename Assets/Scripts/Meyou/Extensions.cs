﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Meyou
{
    /// <summary>
    /// Classe statique comprenant les différentes Extension Methods du namespace Meyou
    /// </summary>
    public static class Extensions
    {
        // Permet de convertir plus facilement une collection de SerializableVector2 en List de
        // Vector2 (et inversement)
        public static List<Vector2> AsVector2(this IEnumerable<SerializableVector2> coll)
        {
            return coll.Select(vec => (Vector2)vec).ToList();
        }

        public static List<SerializableVector2> AsSerializableVector2(this IEnumerable<Vector2> coll)
        {
            return coll.Select(vec => (SerializableVector2)vec).ToList();
        }
    }
}