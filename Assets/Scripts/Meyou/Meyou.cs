using System;
using UnityEngine;

namespace Meyou
{
    public class Meyou : MonoBehaviour
    {
        public Head headPrefab;
        public BodyPart bodyPartPrefab;

        
        /// Material à donner à la tête du Meyou lorsqu'il est animé
        public Material runningHeadMaterial;
        /// Material à donner à la tête du Meyou lorsqu'il est arrêté
        public Material stoppedHeadMaterial;
        

        // Si BuildFrom() a déjà été appelé sur ce Meyou ou non
        private bool _hasBeenBuilt;


        private Head _head;

        private int _nbPositions;
        private float _movementSpeed;
        private float _positionDuration;

        // Données concernant l'animation des Meyoux
        private bool _isRunning;
        private DateTime _startTime;
        private int _animationStep;

        // Ce GameObject (présent dans le Prefab) est positionné
        // par le Meyou au "centre" des Bodyparts, afin d'avoir un GameObject
        // à cibler.
        private GameObject _centerObject;

        // Le Meyou stocke son code génétique pour le faire évoluer + tard
        private Gene _gene;
        public Gene Gene
        {
            get
            {
                if (_gene is null) throw new ApplicationException("Le Meyou n'a pas encore été construit.");
                return _gene;
            }
        }


        // Start is called before the first frame update
        private void Start()
        {
            _centerObject = transform.GetChild(0).gameObject;
        }



        public void BuildFrom(Gene gene, bool start = false)
        {
            if (_hasBeenBuilt) throw new ApplicationException("Tentative de construire un Meyou déjà construit");

            _nbPositions = gene.NbPositions;
            _movementSpeed = gene.MovementSpeed;
            _positionDuration = _movementSpeed / _nbPositions;


            // Construit la tête
            var headGenes = gene.Head;
            _head = SpawnHead(headGenes.GetScale(), gene.NbPositions);


            // Contruit chaque BodyPart connectée à la tête
            foreach (var connection in headGenes.connections)
            {
                var newPartGene = connection.Value.Part;
                var newPart = SpawnPartNextTo(_head, connection.Key, newPartGene.GetScale(), gene.NbPositions);

                _head.AddConnectedBody(connection.Key, newPart, connection.Value.forceFactor);
                _head.AddAnimationStep(connection.Key, connection.Value.Positions.AsVector2());

                BuildChildren(newPart, newPartGene, gene.NbPositions);
                newPart.PopulateConfigJoints();
                newPart.Configure();
            }

            _head.PopulateConfigJoints();
            _head.Configure();
            _gene = gene;
            _hasBeenBuilt = true;

            if (start) StartAnimation();
        }




        /// Renvoie une référence au GameObject représentant la
        /// position du Meyou.
        /// Cela peut-être utile pour suivre le Meyou via une caméra.
        /// Passer par un objet "factice" est nécessaire à cause du fonctionnement
        /// de Unity, c.f GetCenter().
        public GameObject GetCenterObject()
        {
            return _centerObject;
        }




        public void StartAnimation()
        {
            if (_isRunning)
            {
                Debug.LogWarning("Animation is already running!");
                return;
            }

            _head.gameObject.GetComponent<MeshRenderer>().material = runningHeadMaterial;

            _isRunning = true;
            _startTime = DateTime.Now;

            _animationStep = 0;
            SetChildrenPosition(0);
        }


        public void StopAnimation()
        {
            if (!_isRunning)
            {
                Debug.LogWarning("Animation is not running yet.");
                return;
            }
            
            _head.gameObject.GetComponent<MeshRenderer>().material = stoppedHeadMaterial;

            _isRunning = false;
            _animationStep = 0;
            SetChildrenPosition(0);
        }


        /// <summary>
        /// Renvoie la boite qui englobe le Meyou.
        /// </summary>
        public Bounds GetBounds()
        {
            if (!_hasBeenBuilt) return new Bounds(transform.position, Vector3.zero);

            var res = _head.GetComponent<MeshRenderer>().bounds;
            foreach (var bp in GetComponentsInChildren<BodyPart>())
            {
                res.Encapsulate(bp.GetComponent<MeshRenderer>().bounds);
            }

            return res;
        }


        /// <summary>
        /// Défini la visibilité du Meyou. true = visible, false = caché
        /// </summary>
        public void SetVisible(bool visible)
        {
            _head.SetVisibility(visible);
        }





        /// <summary>
        /// Définit récursivement la position cible de
        /// tous les BodyPart du Meyou à l'index donné
        /// </summary>
        private void SetChildrenPosition(int position)
        {
            _head.SetPosition(position);
        }




        /// <summary>
        /// Renvoie la position moyenne de tous les membres de ce Meyou (dans l'espace global).
        /// Si le Meyou n'a pas encore été construit, renvoie la position de son transform.
        /// Ces deux valeurs sont différentes car lorsqu'un Meyou a été construit, ses membres
        /// (ses enfants dans l'arbre de GameObject de Unity) bougent grâce à la physique,
        /// tandis que le Meyou père ne bouge pas.
        /// </summary>
        private Vector3 GetCenter()
        {
            return _hasBeenBuilt ? _head.GetAveragePosition() : transform.position;
        }



        /// <summary>
        /// Construit les enfants du BodyPart en les connectant
        /// grâce au ConfigurableJoint de Bodypart.
        /// </summary>
        private void BuildChildren(BodyPart part, PartGene genes, int nbPositions)
        {
            foreach (var connection in genes.connections)
            {
                var newPartGene = connection.Value.Part;
                var newPart = SpawnPartNextTo(part, connection.Key, newPartGene.GetScale(), nbPositions);

                part.AddConnectedBody(connection.Key, newPart, connection.Value.forceFactor);
                part.AddAnimationStep(connection.Key, connection.Value.Positions.AsVector2());

                BuildChildren(newPart, newPartGene, nbPositions);
                newPart.PopulateConfigJoints();
                newPart.Configure();
            }
        }



        private Head SpawnHead(Vector3 scale, int nbPositions)
        {
            var rotation = Quaternion.LookRotation(Vector3.forward, Vector3.up);

            var head = Instantiate(headPrefab, transform, false);
            var headTransform = head.transform;

            headTransform.localScale = scale;
            headTransform.localRotation = rotation;
            headTransform.localPosition = Vector3.zero;

            head.NbPositions = nbPositions;

            return head;
        }


        /// <summary>
        /// Construit un BodyPart à côté d'un autre Composant.
        /// </summary>
        /// <param name="part">Le Composant auquel "connecter" le nouveau Bodypart</param>
        /// <param name="connector">La face sur laquelle connecter le nouveau BodyPart</param>
        /// <param name="scale">La taille à donner au nouveau BodyPart</param>
        /// <param name="nbPositions">Nombre de positions dans l'animation</param>
        private BodyPart SpawnPartNextTo(Component part, PartPos connector, Vector3 scale, int nbPositions)
        {
            var partTransform = part.transform;
            var bodyPartScale = partTransform.localScale;

            // Calcul la position du BodyPart de manière à ce qu'il soit
            // juxtaposé à son BodyPart "parent" sur la face spécifiée (par connector).
            var delta = connector switch
            {
                PartPos.PosX or PartPos.NegX => bodyPartScale.x,
                PartPos.PosY or PartPos.NegY => bodyPartScale.y,
                PartPos.PosZ or PartPos.NegZ => bodyPartScale.z,
                _ => throw new ArgumentOutOfRangeException(nameof(connector), connector, null)
            };
            delta = (delta + scale.y) / 2;


            var deltaVec = connector switch
            {
                PartPos.PosX => new Vector3(delta, 0, 0),
                PartPos.NegX => new Vector3(-delta, 0, 0),
                PartPos.PosY => new Vector3(0, delta, 0),
                PartPos.NegY => new Vector3(0, -delta, 0),
                PartPos.PosZ => new Vector3(0, 0, delta),
                PartPos.NegZ => new Vector3(0, 0, -delta),
                _ => throw new ArgumentOutOfRangeException(nameof(connector), connector, null)
            };

            var newPartPos = partTransform.position + partTransform.TransformDirection(deltaVec);

            // Le -Y local du nouveau BodyPart doit être dans la direction de la face
            // auquel il est connecté.
            var newPartRotation = Quaternion.FromToRotation(Vector3.up, part.transform.TransformDirection(deltaVec));
            var newPart = Instantiate(bodyPartPrefab, newPartPos, newPartRotation, transform);
            newPart.transform.localScale = scale;


            newPart.NbPositions = nbPositions;


            return newPart;
        }




        // Update is called once per frame
        void Update()
        {
            // _centerObject permet de traquer le centre du Meyou (ex: caméra)
            _centerObject.transform.position = GetCenter();

            if (!_isRunning) return;

            // Change la position cible lorsque le temps est écoulé
            var now = DateTime.Now;

            // on divise par l'échelle de temps
            // pour la prendre en compte
            if ((now - _startTime).Milliseconds > (_positionDuration / Time.timeScale) * 1000)
            {
                _animationStep = (_animationStep + 1) % _nbPositions;
                SetChildrenPosition(_animationStep);
                _startTime = now;
            }
        }
    }
}
