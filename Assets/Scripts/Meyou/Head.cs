using System;
using System.Collections.Generic;
using UnityEngine;

namespace Meyou
{
    public class Head : BodyPart
    {


        /// Défini les positions que doit prendre l'articulation à la position PartPos
        /// au cours de l'animation.
        public override void AddAnimationStep(PartPos connector, List<Vector2> positions)
        {
            if (positions.Count != NbPositions)
                throw new ArgumentException("Le nombre de positions données doit être égal à NbPositions");

            targets.Add(connector, positions);
        }

        public override void PopulateConfigJoints()
        {
            // c.f. Bodypart.PopulateConfigJoints
            if (isConfigured) return;

            var joints = transform.GetComponents<ConfigurableJoint>();
            configJoints.Add(PartPos.PosX, joints[0]);
            configJoints.Add(PartPos.NegX, joints[1]);
            configJoints.Add(PartPos.PosY, joints[2]);
            configJoints.Add(PartPos.NegY, joints[3]);
            configJoints.Add(PartPos.PosZ, joints[4]);
            configJoints.Add(PartPos.NegZ, joints[5]);
        }



        /// Ajoute une connexion à ce BodyPart
        public override void AddConnectedBody(PartPos connector, BodyPart part, float force)
        {
            if (connectedBodies.ContainsKey(connector))
                throw new ArgumentException("Une connexion existe déjà sur ce connecteur");

            connectedBodies.Add(connector, part);
            forces.Add(connector, force);
        }
    }
}
