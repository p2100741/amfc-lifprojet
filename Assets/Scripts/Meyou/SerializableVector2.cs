﻿using System;
using Newtonsoft.Json;
using UnityEngine;

namespace Meyou
{
    /// La classe Vector2 n'étant visiblement pas sérialisable par Json.NET,
    /// on utilise cette classe dans MeyouGene plutôt que Vector2.
    [Serializable]
    public class SerializableVector2 : ICloneable
    {
        public float x;
        public float y;

        public SerializableVector2(Vector2 v)
        {
            x = v.x;
            y = v.y;
        }

        [JsonConstructor] // Constructeur utilisé par Json.NET
        public SerializableVector2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public Vector2 GetVector2()
        {
            return new Vector2(x, y);
        }

        // Convertisseurs implicites pour faciliter l'utilisation de la classe
        public static implicit operator Vector2(SerializableVector2 v)
        {
            return v.GetVector2();
        }

        public static implicit operator SerializableVector2(Vector2 v)
        {
            return new SerializableVector2(v);
        }

        public object Clone()
        {
            return new SerializableVector2(x, y);
        }
    }
}



