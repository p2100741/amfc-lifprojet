﻿using System.Collections.Generic;
using System.Linq;

namespace Stats
{
    public class Overview
    {
        private readonly double _avg;
        private readonly double _min;
        private readonly double _fq;
        private readonly double _med;
        private readonly double _tq;
        private readonly double _max;


        public Overview(IReadOnlyList<double> data)
        {
            _avg = data.Average();
            _min = data.Min();
            _fq = data[data.Count / 4];
            _med = data[data.Count / 2];
            _tq = data[^(data.Count / 4)]; // Index à partir de la fin
            _max = data.Max();
        }

        public double GetAvg() => _avg;
        public double GetMin() => _min;
        public double GetFirstQuartile() => _fq;
        public double GetMedian() => _med;
        public double GetThirdQuartile() => _tq;
        public double GetMax() => _max;
    }
}