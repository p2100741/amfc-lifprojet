﻿using System;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Trials
{
    public class TrialUI : MonoBehaviour
    {
        public TrialManager manager;
        public Image progressionBar;
        public TextMeshProUGUI generationText;
        public TextMeshProUGUI scoreText;

        private bool visible = true;



        public void SavePreviousBest() { manager.SavePreviousBest(); }
        public void Quit() { SceneManager.LoadScene("MainMenu"); }

        public void ChangeVisibility()
        {
            visible = !visible;
            manager.displayAll = visible;
        }



        private void Update()
        {
            UpdateProgressionBar();
            UpdateGenerationText();
            UpdateScoreText();
        }



        private void UpdateProgressionBar()
        {
            var targetWidth = (int)(manager.GetGenerationProgression() * 800);
            var targetPosition = new Vector3(targetWidth / 2, -55);

            progressionBar.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, targetWidth);
            progressionBar.rectTransform.anchoredPosition = targetPosition;
        }



        private void UpdateGenerationText()
        {
            int gen;
            try
            {
                gen = manager.GetGeneration();
            }
            catch
            {
                generationText.text = "En Attente...";
                return;
            }
            var crrntly = manager.GetCurrentlySimulatedMeyouxCount();
            var alrdy = manager.GetAlreadySimulatedMeyouxCount();
            var genSize = manager.generationSize;

            generationText.text = $"Génération {gen} ({alrdy}+{crrntly}/{genSize})";
        }



        private void UpdateScoreText()
        {
            double score;
            try
            {
                score = manager.GetCurrentBestScore();
            }
            catch
            {
                scoreText.text = "";
                return;
            }
            scoreText.text = $"Meilleur score: {score:0.000}";
        }
    }
}