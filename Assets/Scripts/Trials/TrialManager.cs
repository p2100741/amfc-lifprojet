using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Menus;
using Meyou;
using Mutation;
using Newtonsoft.Json;
using Trials.Evaluator;
using UnityEngine;
using Trials.Generators;
using Unity.VisualScripting;

namespace Trials
{
    public enum EvolverType
    {
        Movement,
        BodyAndMovement
    }



    internal enum ManagerStates
    {
        Idle,
        StartupWaiting,
        Evolving
    }


    /// <summary>
    /// Objet orchestrant la phase d'évolution.
    /// </summary>
    public class TrialManager : MonoBehaviour
    {
        /// Nom du fichier .csv à générer contenant les statistiques
        /// du processus d'évolution, génération par génération.
        /// Si laissé vide, les statistiques ne seront pas sauvegardées.
        public string statsOutputFile;

        /// Chemin vers le dossier dans lequel les gène des meilleurs Meyou
        /// de chaque génération seront écrit.
        /// Si laissé vide, aucune snapshot ne sera faite.
        public string snapshotPath;

        /// Nombre de générations entre chaque snapshot.
        /// Ex: si snapshotRate = 10, une snapshot sera faites toutes les 10 générations
        public int snapshotRate = 1;

        /// Nombre de millisecondes à attendre entre Start()
        /// et le début de l'évolution
        public int startupDelay = 500;

        /// Nombre de millisecondes entre l'apparition des Meyoux
        /// et le début de leur évolution. Cela permet qu'ils démarrent
        /// leur animation au sol.
        public int animationStartDelay = 3000;

        // todo: Peut-être en faire une Interface pour avoir une durée en fonction de la génération
        /// Durée de simulation d'une génération, en secondes
        public uint generationDuration = 5;

        /// Quantité de Meyoux à simuler par génération
        public uint generationSize = 100;

        /// Combien de Meyoux d'une même génération
        /// à simuler à la fois.
        /// Ex: si generationSize = 200 et bulkSize = 100,
        /// une génération sera simulée en 2x.
        public uint bulkSize = 100;

        public BodyMutator bodyMutator;
        public MovementMutator movementMutator;

        public MeyouSpawner spawner;
        public MeyouEvaluator evaluator;
        public ConstantGeneGenerator baseGene;
        public DefaultEvolver evolver;


        public bool displayAll = true;


        private int _currentGeneration = -1;
        private uint _toBeSimulated; // nombre de Meyoux de la génération actuel devant encore être simulés

        // Association Gene de Meyou déjà simulé => score du Meyou
        // rempli après chaque groupe de Meyou simulé, jusqu'à avoir simulé toute la génération
        private List<(Gene, double)> _currentGenerationMeyou;
        private Meyou.Meyou[] _currentMeyouBulk;
        private Meyou.Meyou _currentBest;
        private double _currentBestScore;
        private Gene _previousBest;


        private ManagerStates _state;
        private bool _meyouxToBeStarted;
        private bool firstGen;


        private DateTime _evolutionStartDate;
        private DateTime _lastBulkStartDate;


        private StreamWriter _statOutput;




        public void StartEvolution()
        {
            _previousBest = null;
            firstGen = true;

            if (_state == ManagerStates.Evolving) throw new ApplicationException("L'évolution est déjà en marche !");

            // Set-up de la sauvegarde des statistiques
            if (statsOutputFile.Length > 0)
            {
                try
                {
                    _statOutput = File.CreateText(statsOutputFile);
                    _statOutput.AutoFlush = true;
                    _statOutput.Write("Génération|Moyenne|Min|1er Quartile|Mediane|3eme Quartile|Max\n");
                }
                catch
                {
                    _statOutput = null;
                    Debug.LogWarning($"Unable to create file {statsOutputFile}");
                }
            }

            // Set-up de la sauvegarde des meilleurs Meyoux de chaque gen
            if (!Directory.Exists(snapshotPath))
            {
                Debug.LogWarning("Snapshot directory set but does not exists");
                snapshotPath = "";
            }

            _currentGenerationMeyou = new List<(Gene, double)>();
            // Si le gène a été obtenu après x générations, repart à la génération x + 1
            _currentGeneration = baseGene.GetGene(0).Generation;



            _toBeSimulated = generationSize;
            _state = ManagerStates.Evolving;

            StartGeneration(baseGene);
        }



        /// Renvoie une liste des Meyoux actuellement
        /// simulés (le groupe actuel).
        public Meyou.Meyou[] GetMeyoux()
        {
            if (_currentMeyouBulk is null || _currentMeyouBulk.Length == 0)
                throw new ApplicationException("Aucun Meyoux actuellement simulés");

            return _currentMeyouBulk;
        }


        /// <summary>
        /// Renvoie une référence vers l'actuel meilleur Meyou
        /// </summary>
        public Meyou.Meyou GetBestMeyou()
        {
            if (_state == ManagerStates.Evolving) return _currentBest;
            throw new ApplicationException("Pas d'évolution en cours");
        }


        /// <summary>
        /// Renvoie le score de l'actuel meilleur Meyou
        /// </summary>
        public double GetCurrentBestScore()
        {
            if (_state == ManagerStates.Evolving) return _currentBestScore;
            throw new ApplicationException("Pas d'évolution en cours");
        }


        /// Renvoie la génération actuelle, ou une exception si pas encore commencé
        public int GetGeneration()
        {
            if (_currentGeneration < 0)
                throw new ApplicationException("Simulation pas encore commencée");

            return _currentGeneration;
        }



        public int GetAlreadySimulatedMeyouxCount()
        {
            return _currentGenerationMeyou.Count;
        }


        public int GetCurrentlySimulatedMeyouxCount()
        {
            return _currentMeyouBulk.Length;
        }






        /// <summary>
        /// Renvoie la progression actuelle de la génération, entre 0 et 1
        /// (0 = tout début, 1 = fin).
        /// Renvoie 0 si pas actuellement en cours d'évolution
        /// </summary>
        public double GetGenerationProgression()
        {
            if (_state != ManagerStates.Evolving) return 0;

            var currentBulkProgression = (DateTime.Now - _lastBulkStartDate).TotalSeconds / generationDuration;
            var currentBulkWeight = _currentMeyouBulk.Length * 1.0 / generationSize;
            var alreadyDoneProgression = _currentGenerationMeyou.Count * 1.0 / generationSize;

            return alreadyDoneProgression + currentBulkProgression * currentBulkWeight;
        }





        /// <summary>
        /// Sauvegarde le meilleur Meyou de la génération précédente.
        /// Si la génération actuelle est la première, ne fait rien.
        /// </summary>
        public void SavePreviousBest()
        {
            if (_previousBest is null) return;
            SaveFile.SaveGene(_previousBest);
        }




        private void UpdateVisibilities()
        {
            if (_currentMeyouBulk is null) return;

            var best = _currentBest;

            foreach (var meyou in _currentMeyouBulk)
            {
                if (displayAll) meyou.SetVisible(true);
                else if (meyou == best) meyou.SetVisible(true);
                else meyou.SetVisible(false);
            }
        }




        /// Génère soit un nouveau groupe de Meyoux soit de la génération actuelle,
        /// soit une nouvelle génération si besoin.
        /// Utilise le IGeneEvolver donné lors de l'appel à StartEvolution.
        private void RinceAndRepeat()
        {
            // Stocke les scores des Meyoux
            foreach (var meyou in _currentMeyouBulk)
            {
                var meyouEvaluator = meyou.GetComponentInChildren<MeyouEvaluator>();
                var score = meyouEvaluator.GetScore();
                _currentGenerationMeyou.Add((meyou.Gene, score));
            }

            // Supprime les Meyoux
            DestroyMeyoux();

            // Génération finie ou non
            if (_toBeSimulated == 0)
            {
                firstGen = false;
                evolver.SetPreviousGenerationResults(_currentGenerationMeyou);

                _previousBest = GetBestMeyou().Gene;


                // Sauvegarde les statistiques dans le fichier de sortie
                if (_statOutput is not null)
                {
                    var stats = new Stats.Overview(GetScores());

                    var text = string.Format(
                        "{0}|{1}|{2}|{3}|{4}|{5}|{6}",
                        _currentGeneration,
                        stats.GetAvg(),
                        stats.GetMin(),
                        stats.GetFirstQuartile(),
                        stats.GetMedian(),
                        stats.GetThirdQuartile(),
                        stats.GetMax()
                    );
                    _statOutput.Write($"{text}\n");
                }


                // Créé une sauvegarde du meilleur gène de la génération
                if (snapshotPath != "" && _currentGeneration % snapshotRate == 0)
                    WriteSnapshot();

                StartGeneration(evolver);
            }
            else if (firstGen) SpawnBulk(baseGene);
            else SpawnBulk(evolver);
        }





        private void StartGeneration(GeneGenerator generator)
        {
            _currentGenerationMeyou.Clear();
            _currentGeneration += 1;
            _toBeSimulated = generationSize;

            SpawnBulk(generator);
        }




        /// Spawn min(bulkSize, _toBeSimulated)
        /// et diminue _toBeSimulated par le nombre de Meyou créés.
        private void SpawnBulk(GeneGenerator generator)
        {
            var meyouNb = Math.Min(bulkSize, _toBeSimulated);

            _currentMeyouBulk = spawner.SpawnBulk(generationSize - _toBeSimulated, meyouNb, generator, evaluator);
            _toBeSimulated -= meyouNb;

            _currentBest = _currentMeyouBulk[0];
            _currentBestScore = 0.0;

            _lastBulkStartDate = DateTime.Now;
            _meyouxToBeStarted = true;
        }




        /// <summary>
        /// Détruit tous les Meyoux dans _currentMeyouBulk
        /// </summary>
        private void DestroyMeyoux()
        {
            foreach (var m in _currentMeyouBulk) Destroy(m.GameObject());
        }





        // Start is called before the first frame update
        private void Start()
        {
            LoadFromEvolutionLoadInfo();
            _state = ManagerStates.Idle;

            if (generationDuration == 0) throw new ArgumentException("duration doit être > 0");
            if (generationSize <= 1) throw new ArgumentException("generationSize doit être > 1");
            if (bulkSize == 0) throw new ArgumentException("bulkSize doit être > 0");

            // Demande le début de l'évolution dans startupDelay ms.
            _evolutionStartDate = DateTime.Now;
            _state = ManagerStates.StartupWaiting;
        }



        /// <summary>
        /// Charge ses paramètres via la classe statique
        /// EvolutionLoadInfo.
        /// </summary>
        private void LoadFromEvolutionLoadInfo()
        {
            statsOutputFile = EvolutionLoadInfo.StatsOutputFile;
            snapshotPath = EvolutionLoadInfo.SnapshotPath;
            snapshotRate = EvolutionLoadInfo.SnapshotRate;
            generationDuration = EvolutionLoadInfo.GenerationDuration;
            generationSize = EvolutionLoadInfo.GenerationSize;
            bulkSize = EvolutionLoadInfo.BulkSize;

            baseGene.SetGene(EvolutionLoadInfo.InitialGene);

            evolver.mutator = EvolutionLoadInfo.EvolverType switch
            {
                EvolverType.Movement => movementMutator,
                EvolverType.BodyAndMovement => bodyMutator,
                _ => throw new ArgumentOutOfRangeException()
            };
        }



        /// <summary>
        /// Met à jour le meilleur Meyou
        /// </summary>
        private void SetCurrentBest()
        {
            if (_state != ManagerStates.Evolving) return;

            foreach (var m in _currentMeyouBulk)
            {
                var score = m.GetComponentInChildren<MeyouEvaluator>().GetScore();
                if (score <= _currentBestScore) continue;

                _currentBestScore = score;
                _currentBest = m;
            }
        }



        private void Update()
        {
            SetCurrentBest();
            UpdateVisibilities();

            switch (_state)
            {
                case ManagerStates.StartupWaiting:
                    {
                        if (DateTime.Now - _evolutionStartDate > TimeSpan.FromMilliseconds(startupDelay) / Time.timeScale)
                        {
                            StartEvolution();
                        }

                        break;
                    }
                case ManagerStates.Evolving:
                    {
                        if (DateTime.Now - _lastBulkStartDate > TimeSpan.FromSeconds(generationDuration) / Time.timeScale)
                        {
                            RinceAndRepeat();
                        }

                        break;
                    }
            }


            if (_meyouxToBeStarted)
            {
                if (DateTime.Now - _lastBulkStartDate > TimeSpan.FromMilliseconds(animationStartDelay) / Time.timeScale)
                {
                    _meyouxToBeStarted = false;
                    foreach (var meyou in _currentMeyouBulk)
                    {
                        meyou.StartAnimation();
                    }
                }
            }
        }




        /// Ecrit un fichier dans snapshotPath contenant le Gene
        /// de l'actuel meilleur Meyou de la génération
        private void WriteSnapshot()
        {
            var bestGene = _currentGenerationMeyou
                .OrderBy(x => x.Item2)
                .Select(x => x.Item1)
                .Reverse()
                .First();

            var geneStr = JsonConvert.SerializeObject(bestGene);
            var fileName = $"g{_currentGeneration} - {DateTime.Now:MM.dd.yy H.mm.ss}.json";

            using var sw = File.CreateText($@"{snapshotPath}\{fileName}");
            sw.Write(geneStr);
        }




        /// Renvoie un tableau contenant les scores de la
        /// génération actuelle.
        private double[] GetScores()
        {
            return _currentGenerationMeyou
                .Select(x => x.Item2)
                .OrderBy(x => x)
                .ToArray();
        }
    }
}
