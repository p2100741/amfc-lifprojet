using UnityEngine;

namespace Trials
{
    public class CameraController : MonoBehaviour
    {
        //public Meyou.Meyou[] meyoux;
        public TrialManager trialManager;

        //private const float RotationSpeed = 5.0f;
        private const float CameraSpeed = 5.0f;

        private Transform _target;
        private Vector3 _originPos;

        void Start()
        {
            _originPos = transform.position;
        }

        void Update()
        {

            /*//go to the furthest cube from the cube genrator when 0 is pressed
            if (Input.GetKeyDown(KeyCode.Alpha0))
            {
                GoToFurthestObject();
            }

            if (Input.GetKeyDown(KeyCode.Tab))
            {
                CycleThroughObjects();

            }*/

            try { _target = trialManager.GetBestMeyou().GetCenterObject().transform; }
            catch
            {
                // ignored
            }

            MoveCamera();
        }



        /*private void GoToFurthestObject()
        {
            float furthestDistance = 0;
            int furthestIndex = 0;
            for (int i = 0; i < listObjectsOfInterest.Length; i++)
            {
                float distance = Vector3.Distance(listObjectsOfInterest[i].transform.GetChild(0).transform.position,
                    objectGenerator.transform.position);
                if (distance > furthestDistance)
                {
                    furthestDistance = distance;
                    furthestIndex = i;
                }
            }

            _target = listObjectsOfInterest[furthestIndex].transform.GetChild(0).transform;
        }
        private void CycleThroughObjects()
        {


            if (listObjectsOfInterest.Length > 0)
            {
                int currentIndex = -1;

                // Find the index of the current target cube.
                for (int i = 0; i < listObjectsOfInterest.Length; i++)
                {
                    if (listObjectsOfInterest[i].transform == _target)
                    {
                        currentIndex = i;
                        break;
                    }
                }

                // Set the next cube as the target, cycling back to the first cube if needed.
                if (currentIndex == -1 || currentIndex == listObjectsOfInterest.Length - 1)
                {
                    _target = listObjectsOfInterest[0].transform.GetChild(0).transform;
                }
                else
                {
                    _target = listObjectsOfInterest[currentIndex + 1].transform.GetChild(0).transform;
                }

                print(currentIndex);
            }



        }*/

        public void ResetCamera()
        {
            transform.position = _originPos;
        }

        private void MoveCamera()
        {
            if (_target == null) return;

            var targetPosition = _target.position;
            var desiredPosition = _originPos + new Vector3(0, 0, targetPosition.z);

            transform.position = Vector3.Lerp(transform.position, desiredPosition, CameraSpeed * Time.deltaTime);


            // Rotate the camera to look at the cube.
            //Quaternion targetRotation = Quaternion.LookRotation(targetPosition - transform.position);
            //transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);




        }
    }
}
