using System;
using System.Collections.Generic;
using Meyou;
using Trials.Evaluator;
using Trials.Generators;
using UnityEngine;

namespace Trials
{
    /// Classe permettant de faire apparaître un nombre défini de
    /// Meyoux dans une scène. Cette classe met en pause la simulation
    /// durant l'apparition des Meyoux.
    /// Le Spawner doit être placé au niveau du sol.
    /// Les Meyoux seront créés juste au dessus du spawner.
    public class MeyouSpawner : MonoBehaviour
    {
        public Meyou.Meyou meyouPrefab;
        
        private Meyou.Meyou[] _meyoux;
        private bool _spawned;

        private uint _bulkSize; // 0 = pas encore de générations créées


        private void Start()
        {
            _spawned = false;
            _bulkSize = 0;
        }




        /// <summary>
        /// Renvoie la taille de la dernière génération créée.
        /// Renvoie 0 si ce Spawner n'a pas encore créé de génération.
        /// </summary>
        public uint GetBulkSize()
        {
            return _bulkSize;
        }



        /// Fait apparaître un groupe de Meyou avec l'évaluateur donné.
        /// Renvoie une référence vers le tableau des Meyoux créés.
        public Meyou.Meyou[] SpawnBulk(uint startIdx, uint bulkSize, GeneGenerator generator, MeyouEvaluator evaluator)
        {
            if (_spawned)
                throw new ApplicationException("Une génération existe déjà ! Il faut la supprimer d'abord.");

            if (bulkSize == 0) throw new ArgumentException("generationSize doit être > 0");

            // Met en pause la simulation pendant la génération des Meyoux
            var timeScale = Time.timeScale;
            Time.timeScale = 0;

            _bulkSize = bulkSize;
            
            // Liste temporaire pour stocker les Meyoux instanciés;
            // convertie en Array + tard
            var createdMeyoux = new List<Meyou.Meyou> { Capacity = (int) bulkSize };

            for (var i = startIdx; i < startIdx + bulkSize; i++)
                createdMeyoux.Add(SpawnOneMeyou(i, generator, evaluator));

            _meyoux = createdMeyoux.ToArray();


            Time.timeScale = timeScale;
            return _meyoux;
        }




        private Meyou.Meyou SpawnOneMeyou(uint idx, GeneGenerator generator, MeyouEvaluator evaluator)
        {
            var gene = generator.GetGene((int) idx);
            var meyou = SpawnOneRawMeyou(gene);
            
            // Attache l'évaluateur au Meyou
            var eval = Instantiate(evaluator, meyou.transform);
            eval.StartEvaluation(meyou);

            return meyou;
        }



        /// <summary>
        /// Spawn 1 seul Meyou sans passer par un GeneGenerator et sans lui attacher d'evaluator
        /// </summary>
        public Meyou.Meyou SpawnOneRawMeyou(Gene gene)
        {
            var position = transform.position;
            
            var meyou = Instantiate(meyouPrefab, position, Quaternion.identity);
            meyou.BuildFrom(gene);
            
            var minY = meyou.GetBounds().min.y;

            // Place le Meyou à la bonne hauteur
            var meyouTransform = meyou.transform;
            meyouTransform.Translate(0, position.y - minY + 0.1f, 0);

            return meyou;
        }
    }
}
