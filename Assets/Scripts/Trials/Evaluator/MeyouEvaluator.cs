﻿using UnityEngine;

namespace Trials.Evaluator
{
    /// Un objet hériant de cette classe abstraite peut être attaché
    /// aux Meyoux créés par MeyouSpawner afin de calculer leur score.
    public abstract class MeyouEvaluator: MonoBehaviour
    {
        /// Indique à l'évaluateur que l'évaluation du Meyou donné commence.
        public abstract void StartEvaluation(Meyou.Meyou meyou);

        /// Obtient le score du Meyou. Peut être appelé en cours d'évaluation,
        /// ou à la fin pour obtenir le score final.
        public abstract double GetScore();
    }
}