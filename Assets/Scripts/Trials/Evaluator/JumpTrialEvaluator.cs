using System;
using UnityEngine;

namespace Trials.Evaluator
{
    /// Evalue les Meyoux selon les règles suivantes:
    /// - Le score est proportionnel à la distance parcouru par le Meyou sur l'axe Z
    /// - Si le Meyou passe en dessous de Y = Z, son score ne peut plus augmenter pour cette évaluation
    public class JumpTrialEvaluator : MeyouEvaluator
    {
        private const int YFloor = 20;

        private double height;
        private double jump;

        private bool _isEvaluating;

        private Meyou.Meyou _meyou;
        private float _meyouEnergy;
        private double _score;


        // Update is called once per frame
        void Update()
        {
            if (!_isEvaluating) return;
            if (_meyou is null) throw new ApplicationException("Evaluated Meyou not set");

            var yCoo = _meyou.GetCenterObject().transform.position.y;
            
            height = _meyou.GetBounds().size.y;
            jump = _meyou.GetBounds().min.y;

            _score = Math.Max((yCoo - YFloor) / height * 0.1 + (jump - YFloor) * 10, _score);
        }


        public override void StartEvaluation(Meyou.Meyou meyou)
        {
            if (_isEvaluating) throw new ApplicationException("Already evaluating");
            _isEvaluating = true;
            _meyou = meyou;
            _meyouEnergy = meyou.Gene.GetTotalEnergy();
            height = (int)meyou.GetBounds().size.y;

            _score = 0;
        }

        public override double GetScore()
        {
            return _score;
        }
    }
}
