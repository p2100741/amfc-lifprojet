using System;
using UnityEngine;

namespace Trials.Evaluator
{
    /// Evalue les Meyoux selon les règles suivantes:
    /// - Le score est proportionnel à la distance parcouru par le Meyou sur l'axe Z
    /// - Si le Meyou passe en dessous de Y = Z, son score ne peut plus augmenter pour cette évaluation
    public class RangeTrialEvaluator : MeyouEvaluator
    {
        private const int YFloor = 20;

        private bool _isEvaluating;

        private Meyou.Meyou _meyou;
        private float _meyouEnergy;
        private double _score;


        // Update is called once per frame
        void Update()
        {
            if (!_isEvaluating) return;
            if (_meyou is null) throw new ApplicationException("Evaluated Meyou not set");

            var yCoo = _meyou.GetCenterObject().transform.position.y;

            // Elimination du Meyou, sa progression n'est plus comptée.
            if (yCoo <= YFloor)
            {
                _isEvaluating = false;
                _meyou.StopAnimation();

                return;
            }

            /*// Un Meyou qui ne bouge pas est nul
            if (_meyouEnergy == 0)
            {
                _score = 0;
                return;
            }*/

            var zCoo = _meyou.GetCenterObject().transform.position.z;
            var currentScore = zCoo;

            _score = Math.Max(currentScore, _score);
        }


        public override void StartEvaluation(Meyou.Meyou meyou)
        {
            if (_isEvaluating) throw new ApplicationException("Already evaluating");
            _isEvaluating = true;
            _meyou = meyou;
            _meyouEnergy = meyou.Gene.GetTotalEnergy();

            _score = 0;
        }

        public override double GetScore()
        {
            return _score;
        }
    }
}
