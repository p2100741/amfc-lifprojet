﻿using System.Collections.Generic;
using Meyou;

namespace Trials.Generators
{
    /// Fournit un gène en se basant sur le classement d'une
    /// précédente génération
    public abstract class GenerationEvolver : GeneGenerator
    {
        public abstract void SetPreviousGenerationResults(List<(Gene, double)> gen);
    }
}