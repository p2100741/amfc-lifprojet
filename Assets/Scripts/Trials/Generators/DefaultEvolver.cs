using System;
using System.Collections.Generic;
using System.Linq;
using Meyou;
using Mutation;
using Random = System.Random;

namespace Trials.Generators
{
    public class DefaultEvolver : GenerationEvolver
    {
        public GeneMutator mutator;

        private const double C = 1.1f;

        private List<Gene> _evolvedGenes;
        private Random _rdm;


        private void Start()
        {
            _rdm = new Random();
        }


        public override Gene GetGene(int idx)
        {
            return _evolvedGenes[idx];
        }

        public override void SetPreviousGenerationResults(List<(Gene, double)> gen)
        {
            _evolvedGenes = new List<Gene>();

            // Tri les gènes par score décroissant
            var sortedGenes = gen.OrderBy(x => x.Item2).Select(x => x.Item1).Reverse().ToList();
            var nbGene = sortedGenes.Count;

            var tenPercent = nbGene / 10;
            var remaining = nbGene - tenPercent;

            // On garde les 10% meilleur tel quel
            for (var i = 0; i < tenPercent; i++) _evolvedGenes.Add((Gene)sortedGenes[i].Clone());

            // On en choisi 10% d'autre à partir de tous les autres Genes.
            for (var i = 0; i < tenPercent; i++)
            {
                var debugRes = GeneIdxPicker(remaining);
                var idx = debugRes + tenPercent;
                _evolvedGenes.Add((Gene)sortedGenes[idx].Clone());
            }

            // A partir des 20% déjà prélevés, on en génère 80% d'autre en les faisant
            // muter
            remaining = nbGene - tenPercent - tenPercent;

            for (var i = 0; i < remaining; i++)
            {
                var idx = GeneIdxPicker(tenPercent * 2);
                _evolvedGenes.Add(mutator.MutateGene(_evolvedGenes[idx]));
            }


            // Incrémente la génération de chaque gène
            foreach (var gene in _evolvedGenes) gene.Generation += 1;
        }



        /// <summary>
        /// Renvoie un entier entre 0 (inclu) et n (exclu),
        /// avec une chance accru de renvoyer un entier proche de 0.
        /// </summary>
        private int GeneIdxPicker(int n)
        {
            var k = Math.Pow(C, n + 1) - 1;
            var r = Math.Log(_rdm.NextDouble() * k + 1);
            var i = n - (n / Math.Log(k + 1)) * r;
            return (int)i;
        }
    }
}