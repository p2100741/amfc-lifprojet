﻿using Meyou;
using UnityEngine;

namespace Trials.Generators
{
    /// Fournit un gène à assigner à un Meyou
    /// en fonction de son rang dans la génération
    /// (0 => le meilleur)
    public abstract class GeneGenerator: MonoBehaviour
    {
        public abstract Gene GetGene(int idx);
    }
}