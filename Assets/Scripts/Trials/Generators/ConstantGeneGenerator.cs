﻿using Meyou;

namespace Trials.Generators
{
    /// Renvoie toujours le même gène
    public class ConstantGeneGenerator: GeneGenerator
    {
        protected Gene constantGene;

        public void SetGene(Gene gene)
        {
            constantGene = gene;
        }

        public override Gene GetGene(int _)
        {
            return constantGene;
        }
    }
}