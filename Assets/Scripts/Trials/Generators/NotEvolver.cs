using System;
using System.Collections.Generic;
using Meyou;

namespace Trials.Generators
{
    /// Renvoie les mêmes gènes qu'à la génération précédente
    public class NotEvolver : GenerationEvolver
    {
        private List<(Gene, double)> _lastGene;

        public override Gene GetGene(int idx)
        {
            if (_lastGene is null) throw new ApplicationException("Génération précédente non définie");
            return _lastGene[idx].Item1;
        }

        public override void SetPreviousGenerationResults(List<(Gene, double)> gen)
        {
            // Copie profonde des scores
            if (_lastGene is null) _lastGene = new List<(Gene, double)>();
            else _lastGene.Clear();
            foreach (var score in gen)
            {
                _lastGene.Add(((Gene) score.Item1.Clone(), score.Item2));
            }
        }
    }
}
