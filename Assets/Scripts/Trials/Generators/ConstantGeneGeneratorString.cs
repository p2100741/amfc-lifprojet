﻿using System;
using Meyou;
using Newtonsoft.Json;
using System.IO;

namespace Trials.Generators
{
    public class ConstantGeneGeneratorString: ConstantGeneGenerator
    {
        private string gene= File.ReadAllText("Assets/Meyoux/test.json");

        private void Start()
        {
            if (gene == "") throw new ArgumentException("Aucun gène spécifié");
            constantGene = JsonConvert.DeserializeObject<Gene>(gene);
        }
    }
}