using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Meyou;
using Newtonsoft.Json;
using System.IO;


public class MeyouInEditor : MonoBehaviour
{
    private bool isAdding;
    private bool isShapping;
	public GameObject MeyouInEditorPrefab;
	public GameObject UI;
	public GameObject resetButton;
	public GameObject sendMeyouButton;
	public GameObject nbPositionInput;
	private int positionSize = 0; //the size of the position tab
	private GameObject newPartInShapping;
	private RaycastHit button;
	private Gene gene;
    private float scaleX = 1;
	private float scaleY = 1;
    private float scaleZ = 1;
    private string meyouName;
	private PartGene[] partGeneList = new PartGene[100];
	private int partGeneIndex;
    // Start is called before the first frame update
    void Start()
    {
	    Physics.gravity = Vector3.zero;
        isAdding = false;
		isShapping = false;
		UI.SetActive(false);
		sendMeyouButton.SetActive(false);
		resetButton.SetActive(false);
		gene = new Gene(2, 0.6f, "Untitled Meyou");
		partGeneList[0] = new PartGene(1, 1, 1);
		gene.Head = partGeneList[0];
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && isAdding)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit))
            {
				button = hit;
                if (button.collider.CompareTag("FaceDetectorEditor") )
                {
					newPartMode();
					
                }

            }
        }
    }
	
	public void addPosition(string value)
    {
        positionSize = int.Parse(value);
		if (positionSize < 2)
            positionSize =2;
		Destroy(nbPositionInput);
		isAdding = true;
		sendMeyouButton.SetActive(true);
    }

	private void newPartMode()
    {
        isAdding = false;
        isShapping = true;
        UI.SetActive(true);
		resetButton.SetActive(false);
		sendMeyouButton.SetActive(false);
		print (button.transform.forward);
		//instanciate as a child
		GameObject newPart = Instantiate(MeyouInEditorPrefab, button.transform.position, Quaternion.identity, this.transform);
		newPart.transform.localScale = new Vector3(scaleX, scaleY, scaleZ);
		newPart.transform.position = button.transform.position + button.transform.forward *scaleY * 0.5f ;
		newPart.transform.up = button.transform.forward;
		
		partGeneIndex++;
		newPart.name = partGeneIndex.ToString();
		newPartInShapping = newPart;

		
	}
	public void cancel()
    {
        isAdding = true;
        isShapping = false;
        sendMeyouButton.SetActive(true);
        UI.SetActive(false);
        Destroy(newPartInShapping);
		resetButton.SetActive(true);
    }
	
	public void tryValueX(string value)
	{
		if (isShapping)
		{
			scaleX = float.Parse(value);
        	newPartInShapping.transform.localScale = new Vector3(scaleX, newPartInShapping.transform.localScale.y, newPartInShapping.transform.localScale.z);
			
		}	
	}

    public void tryValueY(string value)
    {
		if (isShapping){
        	scaleY = float.Parse(value);
        	newPartInShapping.transform.localScale = new Vector3(newPartInShapping.transform.localScale.x, scaleY, newPartInShapping.transform.localScale.z);
			 newPartInShapping.transform.position = button.transform.position + button.transform.forward *scaleY * 0.5f ;
		}
	}

	public void tryValueZ(string value)
    {
		if (isShapping){
        	scaleZ = float.Parse(value);
        	newPartInShapping.transform.localScale = new Vector3(newPartInShapping.transform.localScale.x, newPartInShapping.transform.localScale.y, scaleZ);
		
        	   
        	
		}	
	}
	public void saveMeyouName(string value)
	{
		meyouName = value;
	}

	public void confirm()
    {
        isAdding = true;
        isShapping = false;
        UI.SetActive(false);
		resetButton.SetActive(true);
		sendMeyouButton.SetActive(true);
		
		var positions = new List<SerializableVector2>();
		for (int i = 0; i< positionSize; i++)
        {
            positions.Add(new Vector2(0, 0));
        }
		partGeneList[partGeneIndex] = new PartGene(scaleX, scaleY, scaleZ);
		switch (button.transform.name)
		{
			case "PosX":
                partGeneList[int.Parse(button.transform.parent.name)].AddConnection(PartPos.PosX, new PartConnection(positions, partGeneList[partGeneIndex], 1.0f));
                break;
            case "NegX":
                partGeneList[int.Parse(button.transform.parent.name)].AddConnection(PartPos.NegX, new PartConnection(positions, partGeneList[partGeneIndex], 1.0f));
                break;
            case "PosY":
                partGeneList[int.Parse(button.transform.parent.name)].AddConnection(PartPos.PosY, new PartConnection(positions, partGeneList[partGeneIndex], 1.0f));
                break;
            case "NegY":
                partGeneList[int.Parse(button.transform.parent.name)].AddConnection(PartPos.NegY, new PartConnection(positions, partGeneList[partGeneIndex], 1.0f));
                break;
            case "PosZ":
                partGeneList[int.Parse(button.transform.parent.name)].AddConnection(PartPos.PosZ, new PartConnection(positions, partGeneList[partGeneIndex], 1.0f));
                break;
            case "NegZ":
                partGeneList[int.Parse(button.transform.parent.name)].AddConnection(PartPos.NegZ, new PartConnection(positions, partGeneList[partGeneIndex], 1.0f));
                break;
		}
		//delete button
	    Destroy(button.transform.gameObject);
		
	}
	
	public void reset()
    {
        isAdding = true;
        isShapping = false;
        UI.SetActive(false);
		for (int i = 0; i < this.transform.childCount; i++)
        {
			if(this.transform.GetChild(i).CompareTag("MeyouPartInEditor"))
            Destroy(this.transform.GetChild(i).gameObject);
        }
    }

	public void saveMeyou()
	{
		gene.Name = meyouName;
		gene.NbPositions = positionSize;
		SaveFile.SaveGene(gene);
	}

	public void GoToTrialScene()
	{
		saveMeyou();

		Physics.gravity = new Vector3(0, -9.81f, 0);
		UnityEngine.SceneManagement.SceneManager.LoadScene("EvolutionConfigMenu");
	}
	
	
        
}
