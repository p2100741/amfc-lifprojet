using UnityEngine;

public class CameraControllerEditor : MonoBehaviour
{
    public GameObject objectOfInterest;
    private Transform target;
    private float horizontalInput;
    private float verticalInput;
    private Vector3 offset = new Vector3(0f, 2f, -5f);

    private float rotationSpeed = 5.0f;
    private bool cameraOrientation; // True = horizontal, False = vertical.

    void Start()
    {
        target = objectOfInterest.transform;
        transform.position = target.position + offset;
        transform.LookAt(target);
        

    }

    void Update()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");

        if (target != null)
        {
            rotateCamera();
        }
    }

    void rotateCamera()
    {
        
        // Calculate the new camera position based on input.
        float horizontalRotationAngle = horizontalInput * 20.0f * rotationSpeed * Time.deltaTime;
        float verticalRotationAngle = verticalInput * 20.0f * rotationSpeed * Time.deltaTime;

        // Rotate around the target horizontally (left/right).
        transform.RotateAround(target.position, Vector3.up, -horizontalRotationAngle);

        // Calculate a new position for the camera after the horizontal rotation.
        Vector3 newPosition = transform.position;
        Vector3 newOrientation = transform.forward;
        // Rotate around the target vertically (up/down).
        transform.RotateAround(target.position, transform.right, verticalRotationAngle);
        // Ensure the camera doesn't flip over by checking the angle between the camera's up vector and the world up vector.
        float angle = Vector3.Angle(transform.up, Vector3.up);


        if (angle > 80)
        {

            transform.position = newPosition; // Restore the previous position to prevent flipping.

            transform.forward = newOrientation; //reset orientation to the target
        }

        
    }
}