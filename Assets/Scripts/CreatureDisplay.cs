using System;
using Newtonsoft.Json;
using Trials.Generators;
using UnityEngine;
using UnityEngine.Animations;

public class CreatureDisplay : MonoBehaviour
{
    private const int AnimationStartDelay = 700;
    private const int SpawnDelay = 200;

    public Trials.MeyouSpawner spawner;
    public Camera cam;
    public string gene;

    private bool _toBeSpawned;
    private bool _toBeStarted;
    private DateTime _startTime;
    private Meyou.Meyou _meyou;
    
    // Start is called before the first frame update
    void Start()
    {
        _toBeStarted = true;
        _toBeSpawned = true;
        _startTime = DateTime.Now;
    }


    private void Update()
    {
        if (_toBeSpawned && DateTime.Now - _startTime > TimeSpan.FromMilliseconds(SpawnDelay))
        {
            var g = JsonConvert.DeserializeObject<Meyou.Gene>(gene);
            _meyou = spawner.SpawnOneRawMeyou(g);
            _toBeSpawned = false;
        }
        
        if (_toBeStarted && DateTime.Now - _startTime > TimeSpan.FromMilliseconds(AnimationStartDelay))
        {
            SetCameraTarget();
            _meyou.StartAnimation();
            _toBeStarted = false;
        }
    }



    private void SetCameraTarget()
    {
        cam.transform.SetParent(_meyou.GetCenterObject().transform);
    }
}
