using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Meyou;
using Newtonsoft.Json;
using UnityEngine;


namespace Mutation
{
    public class BodyMutator : MovementMutator
    {
        /// <summary>
        /// > 0.0 (amplitude of the Size mutation)
        /// </summary>
        public float mutationSizeAmplitude = 0.5f;

        /// <summary>
        /// /// 0.0 - 1.0 (0.0 = no mutation, 1.0 = all genes are mutated)
        /// </summary>
        public float sizeMutationRate = 0.1f;

        /// <summary>
        /// 0.0 - 1.0 (0.0 = no mutation, 1.0 = all genes are mutated)
        /// </summary>
        public float bodyMutationRate = 0.1f;

        public new void Start()
        {
            base.Start();

            // Check if the parameters are correct
            if (mutationSizeAmplitude <= 0.0f)
                throw new ArgumentOutOfRangeException("p_mutationSizeAmplitude", "p_mutationSizeAmplitude must be greater than 0.0");

            if (sizeMutationRate < 0.0f || sizeMutationRate > 1.0f)
                throw new ArgumentOutOfRangeException("p_sizeMutationRate", "p_sizeMutationRate must be between 0.0 and 1.0");

            if (bodyMutationRate < 0.0f || bodyMutationRate > 1.0f)
                throw new ArgumentOutOfRangeException("p_bodyMutationRate", "p_bodyMutationRate must be between 0.0 and 1.0");
        }

        /// <summary>
        /// Mutate a Meyou.Gene and return a new mutated one
        /// </summary>
        /// <param name="p_gene"> The initial Meyou.Gene </param>
        /// <returns> The mutated Meyou.Gene </returns>
        public override Meyou.Gene MutateGene(Meyou.Gene p_gene)
        {
            Meyou.Gene newGene = ApplyMutation(p_gene, MutateSpeed, MutateBodyPartGene);

            return newGene;
        }

        /// <summary>
        /// Mutate a PartGene
        /// </summary>
        /// <param name="partGene"> the PartGene to mutate </param>
        /// <param name="nbPositions">Number of position composing the animation of the Meyou</param>
        /// <returns> the mutated PartGene </returns>
        protected Meyou.PartGene MutateBodyPartGene(Meyou.PartGene PartGene, int nbPositions)
        {
            // mutate the size
            if (_random.NextDouble() < sizeMutationRate)
                PartGene.ScaleX = MutateSize(PartGene.ScaleX);

            if (_random.NextDouble() < sizeMutationRate)
                PartGene.ScaleY = MutateSize(PartGene.ScaleY);

            if (_random.NextDouble() < sizeMutationRate)
                PartGene.ScaleZ = MutateSize(PartGene.ScaleZ);

            // mutate delete connections
            List<int> toDelete = new List<int>();
            foreach (KeyValuePair<Meyou.PartPos, Meyou.PartConnection> connectionDic in PartGene.connections)
            {
                Meyou.PartConnection connection = connectionDic.Value;
                // delete cçonnection
                if (connection.Part.IsLeaf() && _random.NextDouble() < bodyMutationRate)
                {
                    toDelete.Add((int)connectionDic.Key);
                }
            }
            foreach (int key in toDelete)
            {
                PartGene.connections.Remove((Meyou.PartPos)key);
            }

            // mutate add connections
            for (int i = 0; i <= 5 && PartGene.connections.Count < 5; i++)
            {
                if (_random.NextDouble() < bodyMutationRate && !PartGene.connections.ContainsKey((Meyou.PartPos)i) && i != 3)
                {
                    // create a new connection
                    var positions = new List<Meyou.SerializableVector2>();
                    for (int j = 0; j < nbPositions; j++)
                    {
                        positions.Add(new Vector2(0, 0));
                    }

                    var gene = new Meyou.PartGene(0.1f, 0.1f, 0.1f);
                    PartGene.AddConnection((Meyou.PartPos)i, new Meyou.PartConnection(positions, gene, 1.0f));
                }
            }

            // mutate the PartConnection
            PartGene = MutatePartGene(PartGene, nbPositions);
            return PartGene;
        }

        /// <summary>
        /// Mutate a size
        /// </summary>
        /// <param name="size"> the size to mutate </param>
        /// <returns> the mutated size </returns>
        public float MutateSize(float size)
        {
            size += (float)(_random.NextDouble() * mutationSizeAmplitude * 2 - mutationSizeAmplitude);

            if (size < 0.1f)
                size = 0.1f;

            if (size > 10f)
                size = 10f;

            return size;
        }
    }
}