﻿using UnityEngine;

namespace Mutation
{
    public abstract class GeneMutator: MonoBehaviour
    {
        public abstract Meyou.Gene MutateGene(Meyou.Gene pGene);
    }
}