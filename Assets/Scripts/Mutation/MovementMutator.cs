using System;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Random = System.Random;

namespace Mutation
{
    /// <summary>
    /// Mutation class that mutate the movement of a Meyou
    /// </summary>

    public class MovementMutator : RecursiveMutator
    {
        /// <summary>
        /// 0.0 - 1.0 (0.0 = no mutation, 1.0 = all genes are mutated)
        /// </summary>
        public float mutationRate = 0.1f;

        /// <summary>
        /// > 0.0 (amplitude of the Position mutation)
        /// </summary>
        public float movementMutationAmplitude = 5.0f;

        /// <summary>
        /// > 0.0 (amplitude of the Speed mutation)
        /// </summary>
        public float speedMutationAmplitude = 0.5f;

        /// <summary>
        /// > 0.0 (amplitude of the Force mutation)
        /// </summary>
        public float forceMutationAmplitude = 0.1f;


        /// <summary>
        /// Random number generator
        /// </summary>
        protected Random _random;


        protected void Start()
        {
            // Check if the parameters are correct
            if (mutationRate is < 0.0f or > 1.0f)
                throw new ArgumentOutOfRangeException("mutationRate", "mutationRate must be between 0.0 and 1.0");

            if (movementMutationAmplitude <= 0.0f)
                throw new ArgumentOutOfRangeException("movementMutationAmplitude", "movementMutationAmplitude must be greater than 0.0");

            if (speedMutationAmplitude <= 0.0f)
                throw new ArgumentOutOfRangeException("speedMutationAmplitude", "speedMutationAmplitude must be greater than 0.0");

            if (forceMutationAmplitude <= 0.0f)
                throw new ArgumentOutOfRangeException("forceMutationAmplitude",
                    "forceMutationAmplitude must be greater than 0.0");

            _random = new Random();
        }


        /// <summary>
        /// Mutate a Meyou.Gene and return a new mutated one
        /// </summary>
        /// <param name="p_gene"> The initial Meyou.Gene </param>
        /// <returns> The mutated Meyou.Gene </returns>
        public override Meyou.Gene MutateGene(Meyou.Gene p_gene)
        {
            Meyou.Gene newGene = ApplyMutation(p_gene, MutateSpeed, MutatePartGene);
            return newGene;
        }

        /// <summary>
        /// Mutate the speed of a Gene
        /// </summary>
        /// <param name="Gene"> the Gene from witch will the speed be modifie </param>
        /// <returns> The Mutated Gene </returns>
        protected Meyou.Gene MutateSpeed(Meyou.Gene Gene)
        {
            // Check if the speed is mutated
            if (_random.NextDouble() < mutationRate)
            {
                // Mutate the speed with a random value between -_mutationSpeedAmplitude and _mutationSpeedAmplitude

                Gene.MovementSpeed += (float)((_random.NextDouble() * 2f - 1f) * speedMutationAmplitude);
            }
            // Return the mutated speed
            return Gene;
        }

        /// <summary>
        /// Mutate a PartGene
        /// </summary>
        /// <param name="partGene"> the PartGene to mutate </param>
        /// <returns> the mutated PartGene </returns>
        protected Meyou.PartGene MutatePartGene(Meyou.PartGene PartGene, int _)
        {
            foreach (System.Collections.Generic.KeyValuePair<Meyou.PartPos, Meyou.PartConnection> connectionDic in PartGene.connections)
            {
                Meyou.PartConnection PartConnection = connectionDic.Value;
                // Loop in each position of the PartConnection
                foreach (Meyou.SerializableVector2 Position in PartConnection.Positions)
                {

                    // Check if the position x is mutated
                    if (_random.NextDouble() < mutationRate)
                    {
                        // Mutate the position x with a random value between -_mutationAmplitude and _mutationAmplitude
                        Position.x += (float)((_random.NextDouble() * 2f - 1f) * movementMutationAmplitude);
                    }

                    // Check if the position y is mutated
                    if (_random.NextDouble() < mutationRate)
                    {
                        // Mutate the position y with a random value between -_mutationAmplitude and _mutationAmplitude
                        Position.y += (float)((_random.NextDouble() * 2f - 1f) * movementMutationAmplitude);
                    }
                }

                // Check if the force factor is mutated
                if (_random.NextDouble() < mutationRate)
                {
                    PartConnection.forceFactor += (float)((_random.NextDouble() * 2f - 1f) * forceMutationAmplitude);
                }
            }
            return PartGene;
        }
    }
}