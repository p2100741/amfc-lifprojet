using System;
using System.Collections.Generic;
using System.Numerics;

namespace Mutation
{
    /// <summary>
    /// Mutation class that mutate the movement of a Meyou
    /// </summary>
    public abstract class RecursiveMutator : GeneMutator
    {
        public abstract override Meyou.Gene MutateGene(Meyou.Gene p_gene);
        protected Meyou.Gene ApplyMutation(Meyou.Gene Gene, Func<Meyou.Gene, Meyou.Gene> GeneMutator, Func<Meyou.PartGene, int, Meyou.PartGene> PartGeneMutator)
        {
            // Clone the Gene to avoid modifying the original one
            Meyou.Gene newGene = Gene.Clone() as Meyou.Gene;

            // Mutate The gene
            newGene = GeneMutator(newGene);

            newGene.Head = RecursiveMutation(newGene.Head, PartGeneMutator, Gene.NbPositions);

            // Return the mutated Gene
            return newGene;
        }

        /// <summary>
        /// Mutate a Meyou.PartGene and return a new mutated one
        /// </summary>
        /// <param name="p_partGene"> The initial Meyou.PartGene </param>
        /// <returns> The mutated Meyou.PartGene </returns>
        private Meyou.PartGene RecursiveMutation(Meyou.PartGene p_partGene, Func<Meyou.PartGene, int, Meyou.PartGene> PartGeneMutator, int nbPositions)
        {
            // Clone the PartGene to avoid modifying the original one
            Meyou.PartGene newPartGene = p_partGene.Clone() as Meyou.PartGene;

            // Mutate the PartGene
            newPartGene = PartGeneMutator(newPartGene, nbPositions);

            // Loop in each connection of the PartGene
            foreach (System.Collections.Generic.KeyValuePair<Meyou.PartPos, Meyou.PartConnection> connectionDic in newPartGene.connections)
            {
                // Get the PartConnection
                Meyou.PartConnection connection = connectionDic.Value;

                // Recursive call to mutate the next PartGene
                connection.Part = RecursiveMutation(connection.Part, PartGeneMutator, nbPositions);
            }
            // Return the mutated PartGene
            return newPartGene;
        }
    }
}