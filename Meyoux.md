﻿# Explications

## Représentation 

Les meyoux sont des créatures constituées de
pavés droits.

Ils sont représentés sous forme d'arbre: chaque partie
de leur corps est attachée à une autre partie, sauf
la "racine" de l'arbre, que l'on appellera sa "tête".

La classe `MeyouGene` représente le code génétique
d'un individu Meyou. Ces informations sont représentées
sous forme d'arbre.

La classe `Meyou` est un GameObject créé à partir
des informations contenus dans `MeyouGene`.

## Mouvements

Les meyoux se déplacent en répétant le même mouvement,
en boucle. Ce mouvement a une certaine durée et un certain
nombre de positions. Chaque position décrit l'angle que
prennent chacunes des parties du meyou par rapport à
leur partie parente.

Plus le mouvement aura de positions, plus il pourra
être précis, mais plus il représentera de données.